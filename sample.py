import cv2

org = cv2.imread('messi.jpg', 1)
dup = cv2.imread("messiedit.jpg", 1)
#dup = cv2.resize(dup, (1000, 1280))
print("original shape :", org.shape)            #shows no of rows and columns in img in tuple format
print("dup shape :", dup.shape)                 #shows no of rows and columns in img in tuple format
print("original size :", org.size)              #shows no of pixels in img
print("dup size :", dup.size)                   #shows no of pixels in img
#print("original dtype :", org.dtype)            #shows the datatype of img
#print("dup dtype :", dup.dtype)                 #shows the datatype of img

if org.shape == dup.shape:
    diff = cv2.subtract(org, dup)
    cv2.imshow("diff", diff)
    b, g, r = cv2.split(diff)                   #splitting the image to all 3 channels
    cv2.imshow("b", b)
    cv2.imshow("g", g)
    cv2.imshow("r", r)
    if cv2.countNonZero(b) == 0 and cv2.countNonZero(g) == 0 and cv2.countNonZero(r) == 0:
        print("the images are equal")
    else:
        print("images are not equal")





#shows the datatype of img
#b, g, r = cv2.split(img)            #splitting the image to all 3 channels
#img = cv2.merge((b, g, r))          #merges all the channels
#roi = img[120:284, 15:180]
#img[553:717, 610:775] = roi

cv2.imshow('org', org)
cv2.imshow("dup", dup)
cv2.waitKey(0)
#cv2.imwrite('copy.png', img)


#print(img)         #prints in matrix fromat

#to display the image in all 3 channels

#cv2.imshow('blue channel', b)      #shows img in blue channel
#cv2.waitKey(0)
#cv2.imshow('green channel', g)     #shows img in green channel
#cv2.waitKey(0)
#cv2.imshow('red channel', r)       #shows img in red channel
#cv2.waitKey(0)

#to draw geometric shapes in images

#img = cv2.line(img, (0, 0), (130, 130), (255, 0, 0), 10)       #draws a line in the given co-ordinates
#img = cv2.arrowedLine(img, (0, 130), (130, 130), (0, 255, 0), 10)
#img = cv2.rectangle(img, (50, 0), (120, 30), (255, 186, 224), 10)
#img = cv2.circle(img, (140, 60), 30, (255, 0, 0), -1)
#font = cv2.FONT_HERSHEY_SCRIPT_SIMPLEX
#img = cv2.putText(img, 'Demo', (0, 200), font, 2, (0, 0, 255), 10, cv2.LINE_4)

# to play video

#cap = cv2.VideoCapture('rohi.mp4')
#while(1):
#    ret, frame = cap.read()
#    if ret == True:
#        cv2.imshow('video', frame)
#        cv2.waitKey(25)
#    else:
#        break
#cap.release()

